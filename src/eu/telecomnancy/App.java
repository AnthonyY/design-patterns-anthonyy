package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorFactory;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.sensor.TemperatureSensorContext;
import eu.telecomnancy.ui.ConsoleUI;

public class App {

    public static void main(String[] args) {
        ISensor sensor0 = new TemperatureSensor();
        ISensor sensor1 = new TemperatureSensorContext();
        ISensor sensor = SensorFactory.makeSensor();
        new ConsoleUI(sensor);
    }

}