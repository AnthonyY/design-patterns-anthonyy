package eu.telecomnancy.ui;

import java.util.Observer;

public interface SensorObservable {
	void attach(Observer observer);
	void detach(Observer observer);
}
