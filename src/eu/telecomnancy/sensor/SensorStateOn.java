package eu.telecomnancy.sensor;

import java.util.Random;

public class SensorStateOn implements SensorState  {
	
	private double value;

	public boolean getStatus() {
		return true;
	}

	public void update() throws SensorNotActivatedException {
		value = (new Random()).nextDouble() * 100;
		
	}

	public double getValue() throws SensorNotActivatedException {
		return value;
	}
	

}
