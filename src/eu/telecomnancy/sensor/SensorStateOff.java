package eu.telecomnancy.sensor;

public class SensorStateOff implements SensorState {

	public boolean getStatus() {
		return false;
	}

	public void update() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	public double getValue() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}

}
