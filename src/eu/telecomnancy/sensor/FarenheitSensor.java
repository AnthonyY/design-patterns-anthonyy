package eu.telecomnancy.sensor;

public class FarenheitSensor extends SensorDecorator {
	
	private double CeltoFar(double toConvert){
		double converted = toConvert*1.8 + 32; 	
		return 	converted;
	}
	
	@Override
	public double getValue() throws SensorNotActivatedException {
		return CeltoFar(this.sensor.getValue());
	}
}
