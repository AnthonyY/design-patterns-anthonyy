package eu.telecomnancy.sensor;

public class RoundedSensor extends SensorDecorator {

	@Override
	public double getValue() throws SensorNotActivatedException {
		return Math.round(this.sensor.getValue());
	}
}
