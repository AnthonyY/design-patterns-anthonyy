package eu.telecomnancy.sensor;

import java.io.IOException;
import java.util.Properties;

import eu.telecomnancy.helpers.ReadPropertyFile;

public abstract class SensorFactory {
	public static ISensor makeSensor() {
		ReadPropertyFile rp = new ReadPropertyFile();
		Properties p = null;
		try {
			p = rp.readFile("/eu/telecomnancy/app.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String factory = p.getProperty("factory");
		ISensor sensor = null;
		SensorFactory sensorfactory;
			try {
				sensorfactory = (SensorFactory) Class.forName(factory).newInstance();
				sensor = sensorfactory.getSensor();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
		return sensor;
	}
	
public abstract ISensor getSensor();
}
