package eu.telecomnancy.sensor;

public class CelsiusSensor extends SensorDecorator {
	
	private double FartoCel(double toConvert){
		double converted = (toConvert- 32)*1.8; 	
		return 	converted;
	}
	
	@Override
	public double getValue() throws SensorNotActivatedException {
		return FartoCel(this.sensor.getValue());
	}
}
