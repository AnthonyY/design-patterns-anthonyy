package eu.telecomnancy.sensor;

public class SensorProxyFactory extends SensorFactory {

	@Override
	public ISensor getSensor() {
		return new CaptorProxy(new TemperatureSensor());
	}

}
