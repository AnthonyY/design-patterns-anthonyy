package eu.telecomnancy.sensor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CaptorProxy implements ISensor {
	private TemperatureSensor termo;
	
	public CaptorProxy(TemperatureSensor temperatureSensor) {
		this.termo = temperatureSensor;
	}


	public void getLogs(String entry, String result){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date)+": User called" + entry +" which returned : "+result); 

	}
	
	public void on() {
		this.termo.on();
		this.getLogs("On()","sensor turned on");
		
	}

	public void off() {
		this.termo.off();		
		this.getLogs("Off()","sensor turned off");
	}

	public boolean getStatus() {
		String resultLogs ="";
		if (this.termo.getStatus())
			resultLogs = "status is ok";
		else
			resultLogs = "status is failling";
			
		this.getLogs("getStatus()",resultLogs);
		return this.termo.getStatus();
	}

	public void update() throws SensorNotActivatedException {
		this.termo.update();	
	}
	
	public double getValue() throws SensorNotActivatedException {
		double result = this.termo.getValue();
		String resultLogs= String.valueOf(result)+"°C";
		this.getLogs("getValue",resultLogs);
		return result;
	}
}
