package eu.telecomnancy.sensor;

public abstract class SensorDecorator implements ISensor {

	protected ISensor sensor;
	
	public void on() {
		this.sensor.on();
		
	}

	public void off() {
		this.sensor.off();
		
	}

	public boolean getStatus() {
		return this.sensor.getStatus();
	}

	public void update() throws SensorNotActivatedException {
		this.sensor.update();
		
	}

	public double getValue() throws SensorNotActivatedException {
		return this.sensor.getValue();
	}

}
