package eu.telecomnancy.sensor;

public class TemperatureSensorContext implements ISensor {

	private SensorState state;
	
	public void on() {
		this.state = new SensorStateOn();		
	}

	public void off() {
		this.state = new SensorStateOff();
		
	}

	public boolean getStatus() {
		return this.state.getStatus();
	}

	public void update() throws SensorNotActivatedException {
		this.state.update();
		
	}

	public double getValue() throws SensorNotActivatedException {
		return this.state.getValue();
	}

}
